<?php
require('./include/global-vars.php');
require('./include/global-functions.php');
require('./include/config.php');
require('./include/menu.php');

ensure_active_session();

/************************************************
*Constants                                      *
************************************************/
define('LEASES_FILE', '/var/lib/misc/dnsmasq.leases');

/************************************************
*Global Variables                               *
************************************************/
$view = 1;

/************************************************
*Arrays                                         *
************************************************/
$leases = array();                                         //Array of leases from LEASES_FILE


/**
 * Load Active Leases
 *  Load list of systems from LEASES_FILE into $leases array
 *  Use preg_match to make sure it is a valid line
 *  Write relevant $matches as an array to leases[ip]
 *  Active is assumed as yes
 *
 *  @note regex:
 *    Group 1 - Expiry Time in Unix Time - integer
 *    Group 2 - MAC Address
 *    Group 3 - IP Allocated
 *    Group 4 - Device Name
 *    not captured - * or MAC address
 */
function load_activeleases() {
  global $leases;

  $matches = array();

  $fh= fopen(LEASES_FILE, 'r') or die('Error unable to open '.LEASES_FILE);

  while (!feof($fh)) {
    $line = fgets($fh);                                    //Read Line of LogFile

    //Extract component items from log file
    //Create new value in leases by key - IP
    //Value is an array of: mac, name, icon, active
    if (preg_match('/^(\d+) ([\da-f:]{17}) ([\d:\.]+) ([\w\*\-\.]+)/i', $line, $matches)) {
      $leases[$matches[3]] = array('exptime' => $matches[1], 'mac' => $matches[2], 'sysname' => $matches[4], 'sysicon' => 'computer', 'active' => true, 'static' => false);
    }
  }

  fclose($fh);                                             //Close LEASES_FILE
}


/**
 * Add Static Hosts
 *  Add Static Hosts to Leases with additional information which is not present in the log file
 *  1. Read each key and value of $statichost list
 *  2. Check if the IP exists in $leases
 *  2a. If it does, replace the name with the users entry in static_hosts
 *  2b. Otherwise, add system as a new entry and set it as inactive
 *
 */
function add_statichosts() {
  global $config, $leases;

  $hostinfo = array();
  $ip = '';

  foreach($config->dhcp_hosts as $ip => $hostinfo) {
    if (array_key_exists($ip, $leases)) {                  //Does this IP exist in leases array?
      $leases[$ip]['sysname'] = $hostinfo['sysname'];      //Replace the host name
      $leases[$ip]['sysicon'] = $hostinfo['sysicon'];      //Add the icon
      $leases[$ip]['static'] = true;
    }
    else {                                                 //No - add host details as a new entry in leases
      $leases[$ip] = array('exptime' => 0, 'mac' => $hostinfo['mac'], 'sysname' => $hostinfo['sysname'], 'sysicon' => $hostinfo['sysicon'], 'active' => false, 'static' => true);
    }
  }
}


/**
 * Set Default Network Values
 *  Set some default values if an IP address is missing based on the Web server IP
 *
 */
function set_default_network() {
  global $config;

  if (preg_match('/^(\d{1,3}\.\d{1,3}\.\d{1,3}\.)\d{1,3}/', $_SERVER['SERVER_ADDR'], $matches)) {
    $config->dhcp_gateway    = $matches[1].'1';
    $config->dhcp_rangestart = $matches[1].'64';
    $config->dhcp_rangeend   = $matches[1].'254';
  }

  // TODO No idea about IPv6
  elseif (preg_match('/^[0-9a-f:]+/i', $_SERVER['SERVER_ADDR'], $matches)) {
    $config->dhcp_gateway    = $matches[0].'';
    $config->dhcp_rangestart = $matches[0].':00FF';
    $config->dhcp_rangeend   = $matches[0].':FFFF';
  }

  //Not known, use the default values from Dnsmasq
  else {
    $config->dhcp_gateway    = '192.168.0.1';
    $config->dhcp_rangestart = '192.168.0.50';
    $config->dhcp_rangeend   = '192.168.0.150';
  }
}


/**
 * Show Leases
 *  Sort the $leases array using natural sort function natsort
 *  Output the sorted list of $leases into a dhcp-grid
 *  @note buttons and blank template row are added by JavaScript
 *
 */
function show_leases() {
  global $leases;

  $iplist = array();                                      //For sorting $leases
  $cell_expiry = '';
  $cell_icon = '';
  $cell_ip = '';
  $cell_mac = '';
  $cell_sysname = '';
  $cell_static = '';
  $rowclass = '';
  $currenttime = 0;

  $currenttime = time();

  echo '<div id="tab-content-1">', PHP_EOL;               //Start Tab
  echo '<h5>DHCP Leases</h5>', PHP_EOL;

  if (count($leases) == 0) {                              //Any devices in array?
    echo '<h4>&#128551; No devices found</h4>', PHP_EOL;
    echo '</div>', PHP_EOL;                               //End Tab
    return;
  }

  //Get list of IP Addresses and carry out a natural sort
  $iplist = array_keys($leases);
  natsort($iplist);

  echo '<div id="hoststable" class="grid-table">', PHP_EOL;

  //Output sorted iplist into a grid table
  foreach ($iplist as $ip) {
    $rowclass = $leases[$ip]['active'] ? 'class="dhcp-grid"' : 'class="dhcp-grid grey"';
    $cell_icon = "data-device='device-{$leases[$ip]['sysicon']}'";
    $cell_ip = "<div contenteditable='true'>{$ip}</div>";
    $cell_mac = "<div contenteditable='true'>{$leases[$ip]['mac']}</div>";
    $cell_sysname = "<div contenteditable='true'>{$leases[$ip]['sysname']}</div>";

    //Make sure expired time below current time is shown as 'Expired'
    if ($leases[$ip]['exptime'] > $currenttime) {
      $cell_expiry = date("d M \- H:i:s", $leases[$ip]['exptime']);
    }
    else {
      $cell_expiry = 'Expired';
    }

    if ($leases[$ip]['static']) {
      $cell_static = '<input type="checkbox" checked="checked" title="Set Static IP">';
    }
    else {
      $cell_static = '<input type="checkbox" title="Set Static IP">';
    }

    //Output the grid row
    echo "<div {$rowclass}><div {$cell_icon}></div><div>{$cell_sysname}</div><div>{$cell_ip}</div><div>{$cell_mac}</div><div>{$cell_expiry}</div><div>{$cell_static}</div><div></div></div>", PHP_EOL;
  }

  //Save button
  echo '<div class="dhcp-grid"><div><button class="material-icon-button-text icon-tick" type="button" onclick="submitForm(1)">Save Changes</button></div></div>', PHP_EOL;

  echo '</div>', PHP_EOL;                                 //End grid-table
  echo '</div>', PHP_EOL;                                 //End Tab
}


/**
 * Show DHCP Config
 *
 */
function show_dhcpconfig() {
  global $config;

  echo '<div id="tab-content-2">', PHP_EOL;               //Start Tab 2
  echo '<h5>DHCP Config</h5>', PHP_EOL;
  echo '<table class="conf-table">';

  $help = '<i>Turn <a href="https://en.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol">DHCP</a> on or off.</i>';
  echo '<tr><td>Enabled</td><td><input type="checkbox" name="enabled" id="enabledBox" ', is_checked($config->dhcp_enabled), '>', $help, '</td></tr>', PHP_EOL;

  $help = '<i>Authoritative mode will barge in and take over the lease for any client which broadcasts on the network.</i>';
  echo '<tr id="confRow1"><td>Authoritative:</td><td><input type="checkbox" name="authoritative"', is_checked($config->dhcp_authoritative), '>',$help, '</td></tr>', PHP_EOL;

  $help = '<i>Usually the IP address of your Router.</i>';
  echo '<tr id="confRow2"><td>Gateway IP:</td><td><input type="text" name="gateway_ip" placeholder="192.168.0.1" value="', $config->dhcp_gateway, '">', $help, '</td></tr>', PHP_EOL;

  $help = '<i>IP address to start the DHCP range</i>';
  echo '<tr id="confRow3"><td>Range - Start IP:</td><td><input type="text" name="start_ip" placeholder="192.168.0.150" value="'.$config->dhcp_rangestart.'">', $help, '</td></tr>', PHP_EOL;

  $help = '<i>IP address to end the DHCP range</i>';
  echo '<tr id="confRow4"><td>Range - End IP:</td><td><input type="text" name="end_ip" placeholder="192.168.0.150" value="'.$config->dhcp_rangeend.'">', $help, '</td></tr>', PHP_EOL;

  $help = '<i>Length of time the DHCP allocation is valid for.</i>';
  echo '<tr id="confRow5"><td>Lease Time:</td><td><input type="text" name="lease_time" placeholder="24h" value="'.$config->dhcp_leasetime.'">', $help, '</td></tr>', PHP_EOL;

  echo '<tr><td colspan="2"><button class="material-icon-button-text icon-tick" type="button" onclick=submitForm(2)>Save Changes</button></td></tr>', PHP_EOL;
  echo '</table>', PHP_EOL;
  echo '</div>', PHP_EOL;                                 //End Tab
}


/**
 * Show Basic View
 *  Just show the config page when no leases file can be found
 *  Requires displaying the dhcpForm, then call show_dhcpconfig
 *
 */
function show_basicview() {
  echo '<form id="dhcpForm" action="?" method="post">', PHP_EOL;
  echo '<section>', PHP_EOL;
  echo '<p>DHCP is not currently handled by NoTrack.<br>', PHP_EOL;
  echo 'Enable it in the config below:-</p>', PHP_EOL;

  show_dhcpconfig();

  echo '</section>', PHP_EOL;
  echo '</form>', PHP_EOL;                                //End Form
}


/**
 * Draw Tabbed View
 *
 *  @param int Tab to View
 *
 */
function draw_tabbedview($view) {
  $tab = filter_integer($view, 1, 2, 1);
  $checkedtabs = array('', '', '');
  $checkedtabs[$tab] = ' checked';

  echo '<form id="dhcpForm" action="?" method="post">', PHP_EOL;
  echo '<input type="hidden" id="viewTab" name="view" value="'.$tab.'">', PHP_EOL;
  echo '<input type="hidden" id="newHosts" name="newhosts" value="">', PHP_EOL;

  echo '<section>', PHP_EOL;
  echo '<div id="tabbed">', PHP_EOL;                      //Start tabbed container

  echo '<input type="radio" name="tabs" id="tab-nav-1"', $checkedtabs[1], '><label for="tab-nav-1">DHCP Leases</label>', PHP_EOL;
  echo '<input type="radio" name="tabs" id="tab-nav-2"', $checkedtabs[2], '><label for="tab-nav-2">DHCP Config</label>', PHP_EOL;

  echo '<div id="tabs">', PHP_EOL;                        //Start Tabs

  show_leases();
  show_dhcpconfig();

  echo '</div>', PHP_EOL;                                 //End tabs
  echo '</div>', PHP_EOL;                                 //End tabbed container
  echo '</section>', PHP_EOL;
  echo '</form>', PHP_EOL;                                //End form
}


/**
 * Update Static Hosts Array
 *  Process newhosts POST data which was built from JS function submitForm
 *  1. Check if data has been posted to newhosts
 *  2. Explode the new line seperated data from newhosts into an array
 *  3. Carry out basic regex check on each line to avoid XSS vulnerabilities
 *     Further filtering of add_host is done in config
 *
 *  @note Regex:
 *  Group 1: IPv4 or IPv6 address
 *  Group 2: MAC Address
 *  Group 3: Name (optional)
 *  Group 4: Device Icon
 */
function update_statichosts() {
  global $config;

  $matches = array();
  $newhosts = array();
  $host = '';
  $ip = '';
  $regex_newhost = '/^\s*([\d\.:]+)\s*,\s*((?:[\dA-Fa-f]{2}:){5}[\dA-Fa-f]{2})\s*,\s*([\w\.\-]+)?\s*,\s*device\-([a-z]{2,15})/';

  $config->dhcp_clearhosts();

  if (! isset($_POST['newhosts'])) return;                 //Leave if there is nothing in newhosts

  $newhosts = json_decode($_POST['newhosts']);             //Decode JSON data into an array

  foreach($newhosts as $host) {
    if (preg_match($regex_newhost, $host, $matches)) {     //Ensure line is valid
      //Further filtering is done in $config
      $config->dhcp_addhost($matches[1], $matches[2], $matches[3], $matches[4]);
    }
  }
}


/**
 * Update DHCP
 *  Assign POST items to $config->dhcp values
 *  Validation is carried out by $config
 *
 */
function update_dhcp() {
  global $config;

  $config->dhcp_enabled = isset($_POST['enabled']);
  $config->dhcp_authoritative = isset($_POST['authoritative']);

  if (isset($_POST['gateway_ip'])) {
    $config->dhcp_gateway = $_POST['gateway_ip'];
  }

  if (isset($_POST['start_ip'])) {
    $config->dhcp_rangestart = $_POST['start_ip'];
  }

  if (isset($_POST['end_ip'])) {
    $config->dhcp_rangeend = $_POST['end_ip'];
  }

  if (isset($_POST['lease_time'])) {
    $config->dhcp_leasetime = $_POST['lease_time'];
  }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="./css/master.css" rel="stylesheet" type="text/css">
  <link href="./css/icons.css" rel="stylesheet" type="text/css">
  <link href="./css/tabbed.css" rel="stylesheet" type="text/css">
  <link rel="icon" type="image/png" href="./favicon.png">
  <script src="./include/menu.js"></script>
  <title>NoTrack - Home Network</title>
</head>

<body>
<?php
draw_page_header('Network');
draw_page_nav();
echo '<main>', PHP_EOL;

load_serversettings();                                     //Load DHCP Settings

if (count($_POST) > 2) {                                   //Anything in POST array?
  //Set value for view from POST value
  if (isset($_POST['view'])) {
    $view = filter_integer($_POST['view'], 1, 2, 1);
  }

  //Update statichosts and DHCP config based on users input
  update_statichosts();
  update_dhcp();
  $config->save_serversettings();
}

//Set value for view from GET value
if (isset($_GET['view'])) {
  $view = filter_integer($_GET['view'], 1, 2, 1);
}

//Check if any IP settings are blank
if (($config->dhcp_gateway == '') || ($config->dhcp_rangestart == '') || ($config->dhcp_rangeend == '')) {
  set_default_network();
}

//Is DHCP active and handled by NoTrack?
if (file_exists(LEASES_FILE)) {
  load_activeleases();                                     //Load LEASES_FILE
  add_statichosts();                                       //Add static hosts to leases
  draw_tabbedview($view);
}
//DHCP not active, fallback to showing only the DHCP config
else {
  show_basicview();
}

?>
</main>
<script>
/**
 * Create an Add Button for Static Hosts table
 *  @param object Cell to insert Button into
 *
 */
function createAddButton(parentCell) {
  let newButton = document.createElement('button');
  newButton.className = 'button-grey material-icon-centre icon-plus';
  newButton.innerHTML = '&nbsp;';
  newButton.type = 'button';
  newButton.addEventListener('click', addRow);
  parentCell.appendChild(newButton);
}

/**
 * Create Delete Button for Static Hosts table
 *  @param object Cell to insert Button into
 *
 */
function createDeleteButton(parentCell) {
  let newButton = document.createElement('button');
  newButton.className = 'button-grey material-icon-centre icon-delete';
  newButton.innerHTML = '&nbsp;';
  newButton.type = 'button';
  newButton.addEventListener('click', deleteRow);
  parentCell.appendChild(newButton);
}

/**
 * Create Device Menu
 *  Creates a Button with attached UL populated with device icons
 *  @param object Cell to insert Device Menu into
 *
 */
function createDeviceMenu(parentCell) {
  let buttonClass = 'device-computer';
  let newUL = document.createElement('ul');
  let newButton = document.createElement('button');

  if (parentCell.dataset.hasOwnProperty('device')) {
    buttonClass = parentCell.dataset.device;
  }

  newButton.appendChild(newUL);
  newButton.className = buttonClass;
  newButton.type = 'button';
  parentCell.appendChild(newButton);

  createDeviceLI(newUL, 'device-computer', 'Computer');
  createDeviceLI(newUL, 'device-laptop', 'Laptop');
  createDeviceLI(newUL, 'device-server', 'Server');
  createDeviceLI(newUL, 'device-nas', 'NAS');
  createDeviceLI(newUL, 'device-phone', 'Phone');
  createDeviceLI(newUL, 'device-raspberrypi', 'Raspberry Pi');
  createDeviceLI(newUL, 'device-tv', 'TV');
}

/**
 * Create Device Item
 *  Creates a List Item for Device Menu
 *  @param object UL to attach new LI to
 *  @param str Device Class Name
 *  @param str Title
 *
 */
function createDeviceLI(parentUL, deviceName, deviceTitle) {
  let newLI = document.createElement('li');

  newLI.className = deviceName;
  newLI.title = deviceTitle;
  newLI.addEventListener('click', setDeviceIcon);
  parentUL.appendChild(newLI);
}

/**
 * Add Row to Static Hosts table
 *  1. Make the Add button a Del button
 *  2. Insert a new Table row 1 up from the end (above Save Changes button)
 *  3. Create and populate cells in new row
 *
 */
function addRow() {
  const tbl = document.getElementById('hoststable');

  //Make the current Add button a Delete button
  this.className = 'button-grey material-icon-centre icon-delete';
  this.removeEventListener('click', addRow);
  this.addEventListener('click', deleteRow);

  let newRow = document.createElement('div');
  newRow.className = "dhcp-grid";
  newRow.innerHTML = "<div></div><div></div><div></div><div></div><div></div><div><input type='checkbox' checked='checked'></div><div></div>";
  createDeviceMenu(newRow.children[0]);
  createAddButton(newRow.children[6]);
  newRow.children[1].innerHTML = '<div contenteditable="true" placeholder="new.host">';
  newRow.children[2].innerHTML = '<div contenteditable="true" placeholder="192.168.0.3">';
  newRow.children[3].innerHTML = '<div contenteditable="true" placeholder="11:22:33:aa:bb:cc">';
  tbl.insertBefore(newRow, tbl.children[tbl.children.length - 1]);
}

/**
 * Delete Row
 *  Get the table cell for the button which was pressed
 *  Delete the parent of the cell (the table row)
 *
 */
function deleteRow() {
  const p = this.parentNode.parentNode;
  p.parentNode.removeChild(p);
}


/**
 * Set Device Icon
 *  Change the button class to show the image user has selected from dropdown menu
 *
 */
function setDeviceIcon() {
  const p = this.parentNode.parentNode;
  p.className = this.className;
  p.blur();
}


/**
 * Setup Hosts Table
 *  1. Create a Device Menu and Delete Button for each row
 *  2. Add keypress event listener to tick Static IP when contenteditable divs are changed
 *  3. Create a template blank row
 *
 */
function setupHostsTable() {
  const tbl = document.getElementById('hoststable');

  //tbl won't exist when leases file is not present
  if (tbl == null) {
    return;
  }

  const rowCount = tbl.children.length - 1;

  for (let i = 0; i < rowCount; i++) {
    createDeviceMenu(tbl.children[i].children[0]);
    createDeleteButton(tbl.children[i].children[6]);
    tbl.children[i].children[1].children[0].addEventListener('keypress', updateRow);
    tbl.children[i].children[2].children[0].addEventListener('keypress', updateRow);
    tbl.children[i].children[3].children[0].addEventListener('keypress', updateRow);
  }

  let newRow = document.createElement('div');
  newRow.className = "dhcp-grid";
  newRow.innerHTML = "<div></div><div></div><div></div><div></div><div></div><div><input type='checkbox' checked='checked'></div><div></div>";
  createDeviceMenu(newRow.children[0]);
  createAddButton(newRow.children[6]);
  newRow.children[1].innerHTML = '<div contenteditable="true" placeholder="new.host">';
  newRow.children[2].innerHTML = '<div contenteditable="true" placeholder="192.168.0.3">';
  newRow.children[3].innerHTML = '<div contenteditable="true" placeholder="11:22:33:aa:bb:cc">';
  tbl.insertBefore(newRow, tbl.children[rowCount]);
}


/**
 * Submit Form
 *  Collect contenteditable data then add it as a JSON encoded value to newHosts
 *  1. tbl element (the contenteditable table) may be null if show_basicview is used
 *  2. Extract the device icon class from button className
 *  3. Extract other values using innerText
 *  4. Add as an array to hostList
 *  5. JSON Encode hostlist
 *  6. Submit form
 *
 *  @param int Tab to return to
 *
 */
function submitForm(returnTab) {
  const tbl = document.getElementById('hoststable');
  let deviceIcon = '';
  let hostList = Array();
  let host = '';

  //tbl won't exist when leases file is not present
  if (tbl === null) {
    document.getElementById('dhcpForm').submit();
    return;
  }

  const rowCount = tbl.children.length - 1;

  for (let i = 0; i < rowCount; i++) {
    //Ignore unticked Static IP checkboxes
    if (tbl.children[i].children[5].children[0].checked === false) continue;

    //Row > Div > Button > Button Class Name
    deviceIcon = tbl.children[i].children[0].children[0].className;
    //Set the elemets for host based on the row cells
    host = tbl.children[i].children[2].innerText + ',';   //IP
    host += tbl.children[i].children[3].innerText + ',';  //MAC
    host += tbl.children[i].children[1].innerText + ',';  //Name
    host += deviceIcon + '\n';                            //Device Icon
    hostList.push(host);
  }

  //console.log(hostList);
  document.getElementById('newHosts').value = JSON.stringify(hostList);
  document.getElementById('viewTab').value = returnTab;
  document.getElementById('dhcpForm').submit();           //Submit the form data
}


/**
 * Update Row
 *  Set Static IP checkbox to ticked
 *
 */
function updateRow() {
  const parentRow = this.parentNode.parentNode;
  parentRow.children[5].children[0].checked = true;
}

/**
 * Hide Config
 *  Hide the few rows below DHCP Enabled Tickbox
 *
 */
function hideConfig() {
  document.getElementById('confRow1').style.display = 'none';
  document.getElementById('confRow2').style.display = 'none';
  document.getElementById('confRow3').style.display = 'none';
  document.getElementById('confRow4').style.display = 'none';
  document.getElementById('confRow5').style.display = 'none';
}


/**
 * Show Config
 *  Show the few rows below DHCP Enabled Tickbox
 *
 */
function showConfig() {
  document.getElementById('confRow1').style.display = '';
  document.getElementById('confRow2').style.display = '';
  document.getElementById('confRow3').style.display = '';
  document.getElementById('confRow4').style.display = '';
  document.getElementById('confRow5').style.display = '';
}


//Event listener to hide dhcp config when it is disabled
document.getElementById('enabledBox').addEventListener('change', (event) => {
  if (event.target.checked) {
    showConfig();
  } else {
    hideConfig();
  }
});

window.onload = function() {
  if (! document.getElementById('enabledBox').checked) {
    hideConfig();
  }
  setupHostsTable();
};
</script>
</body>
</html>
