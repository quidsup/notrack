<?php
//There are two views for upgrade:
//1. Carrying out upgrade (dependant on POST['doupgrade']) which shows the result of ntrk-upgrade
//2. Version info, upgrade button, and curl output

require('./include/global-vars.php');
require('./include/global-functions.php');
require('./include/config.php');
require('./include/upgradenotifier.php');
require('./include/menu.php');

ensure_active_session();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="./css/master.css" rel="stylesheet" type="text/css">
  <link href="./css/icons.css" rel="stylesheet" type="text/css">
  <link rel="icon" type="image/png" href="./favicon.png">
  <script src="./include/menu.js"></script>
  <title>NoTrack - Upgrade</title>
</head>

<body>
<?php
draw_page_header('Upgrade');
draw_page_nav();

echo '<main>', PHP_EOL;
echo '<section>', PHP_EOL;
echo '<h5>NoTrack Upgrade</h5>', PHP_EOL;
echo '<table class="conf-table">', PHP_EOL;
if ($upgradenotifier->is_upgrade_available()) {
  echo '<tr><td>Status</td><td>Running version v', VERSION, '<br>Latest version available: v', $upgradenotifier->latestversion, '</td></tr>', PHP_EOL;
}
else {
  echo '<tr><td>Status</td><td>Running the latest version v', VERSION, '</td></tr>', PHP_EOL;
}
echo '</table>'.PHP_EOL;
echo '<h6>Note:</h6>';
echo '<p>Due to security reasons, the ability to upgrade via the web interface has been removed.<br>', PHP_EOL;
echo 'Please manually upgrade</p>', PHP_EOL;
echo '<pre>', PHP_EOL;
echo '$ cd ~/notrack/src', PHP_EOL;
echo '$ <span class="blue">#or</span>', PHP_EOL;
echo '$ cd /opt/notrack/src', PHP_EOL;
echo '$ sudo python3 ntrkupgrade.py', PHP_EOL;
echo '</pre>', PHP_EOL;
echo '</section>', PHP_EOL;


//Display changelog
if (extension_loaded('curl')) {                          //Check if user has Curl installed
  $ch = curl_init();                                     //Initiate curl
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
  curl_setopt($ch, CURLOPT_URL,'https://gitlab.com/quidsup/notrack/raw/master/changelog.txt');
  $data = curl_exec($ch);                                //Download Changelog
  curl_close($ch);                                       //Close curl
  echo '<pre>'.PHP_EOL;
  echo '<b>Changelog:</b>', PHP_EOL;
  echo $data;                                            //Display Changelog
  echo '</pre>'.PHP_EOL;
}

?>
</main>
</body>
</html>
