<?php
require('./include/global-vars.php');
require('./include/global-functions.php');
require('./include/mysqlidb.php');
require('./include/config.php');
require('./include/upgradenotifier.php');
require('./include/menu.php');

ensure_active_session();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=0.9">
  <link href="./css/master.css" rel="stylesheet" type="text/css">
  <link href="./css/icons.css" rel="stylesheet" type="text/css">
  <link href="./css/chart.css" rel="stylesheet" type="text/css">
  <link href="./css/home.css" rel="stylesheet" type="text/css">
  <link rel="icon" type="image/png" href="./favicon.png">
  <script src="./include/menu.js"></script>
  <title>NoTrack Admin</title>
</head>

<body>
<?php
/************************************************
*Constants                                      *
************************************************/
$CHARTCOLOURS = array('#00c2c8', '#b1244a', '#93a0ff');

/************************************************
*Global Variables                               *
************************************************/
$dbwrapper = new MySqliDb;

$day_allowed = 0;
$day_blocked = 0;
$allowed_queries = array();
$blocked_queries = array();
$chart_labels = array();
$link_labels = array();

/********************************************************************
 *  Block List Box
 *    Show the blocklist count
 *
 */
function home_blocklist() {
  global $dbwrapper;

  $data = $dbwrapper->count_blocklisted_domains();

  echo '<a href="./config/blocklists.php">',
    '<h2>Block List</h2>',
    '<h3>', number_format(floatval($data['total'])), '</h3>',
    '<p>Domains</p></a>', PHP_EOL;
}


/********************************************************************
 *  DHCP Network Box
 *    Check if DHCP has been enabled with the presence of dnsmasq.leases
 *    Read number of lines from dnsmasq.leases using wc
 *    Alternative is to guide user to setup page
 *
 */
function home_network() {
  $lines = 0;

  if (file_exists('/var/lib/misc/dnsmasq.leases')) {       //DHCP Active
    $lines = number_format(floatval(exec('wc -l /var/lib/misc/dnsmasq.leases | cut -d\  -f 1')));
    echo '<a href="./dhcp.php">',
      '<h2>Network</h2>',
      '<h3>', $lines, '</h3>',
      '<p>Systems</p></a>', PHP_EOL;
  }
  else {                                                   //DHCP Disabled
    echo '<a href="./dhcp.php">',
      '<h2>Network</h2>',
      '<h3>0</h3>',
      '<p>Click here to setup</p></a>', PHP_EOL;
  }
}


/********************************************************************
 *  DNS Queries Box
 *    Display Allowed and Blocked query totals
 *
 *  Params:
 *    day_allowed (int)
 *    day_blocked (int)
 *  Return:
 *    None
 */
function home_queries($day_allowed, $day_blocked) {
  //Allowed Queries
  echo '<a href="./queries.php">',
    '<h2>Allowed Queries</h2>',
    '<h3>', number_format(floatval($day_allowed)), '</h3>',
    '</a>', PHP_EOL;

  //Blocked Queries
  echo '<a href="./queries.php?severity=6">',
    '<h2>Blocked Queries</h2>',
    '<h3>', number_format(floatval($day_blocked)), '</h3>',
    '</a>', PHP_EOL;
}


/********************************************************************
 *  Status Box
 *    Look at the file modified time for NoTrack list file under /etc/dnsmasq

 *  Params:
 *    None
 *  Return:
 *    None
 */
function home_status() {
  global $config;

  $bgcolour = '';                                          //Class for background colour
  $date_msg = '';
  $date_submsg = '';
  $days_old = 0;                                           //File modified time days ago
  $mtime = 0;                                              //File modified time in epoch

  if (file_exists(NOTRACK_LIST)) {                         //Does notrack.list exist?
    $mtime = filemtime(NOTRACK_LIST);                      //Get last modified time
    $days_old = floor((time() - $mtime) / 86400);          //Calc epoch time difference in days

    //Display appropriate message based on age of list file
    if ($days_old <= 0) {                                  //Today
      $date_msg = '<h3 class="green">Today</h3>';
      $date_submsg = 'Block list is in date';
    }
    elseif ($days_old == 1) {                              //Yesterday
      $date_msg = '<h3 class="green">Yesterday</h3>';
      $date_submsg = 'Block list is in date';
    }
    elseif ($days_old < 5) {                               //2 - 4 Days
      $date_msg = '<h3>'.$days_old.' Days</h3>';
      $date_submsg = 'Block list is in date';
    }
    elseif ($days_old < 7) {                               //5 - 6 Days is getting stale
      $bgcolour = ' class="home-bgyellow"';
      $date_msg = '<h3 class="darkgray">'.$days_old.' Days</h3>';
      $date_submsg = 'Out of date';
    }
    elseif ($days_old < 12) {                              //7 to 11 Days
      $bgcolour = ' class="home-bgyellow"';
      $date_msg = '<h3>Last Week</h3>';
      $date_submsg = 'Out of date';
    }
    else {                                                 //Beyond 2 weeks is too old
      $bgcolour = ' class="home-bgred"';
      $date_msg = '<h3>'.date('d M', $mtime).'</h3>';
      $date_submsg = 'Out of date';
    }
  }
  //Alternative is to check status if list file is missing
  elseif ($config->status & STATUS_ENABLED) {              //Enabled with missing list
    $date_msg = '<h3 class="darkgray">Unknown</h3>';
    $date_submsg = 'Block List Missing';
    $bgcolour = ' class="home-bgred"';
  }
  elseif ($config->status & STATUS_DISABLED) {             //Disabled
    $date_msg = '<h3>N/A</h3>';
    $date_submsg = 'Blocking Disabled';
  }
  elseif ($config->status & STATUS_PAUSED) {               //Paused
    $date_msg = '<h3>N/A</h3>';
    $date_submsg = 'Blocking Paused';
  }

  //Display result
  echo "<div$bgcolour>",
    '<h2>Last Updated</h2>',
    $date_msg,
    "<p>$date_submsg</p>",
    '</div>', PHP_EOL;
}


/********************************************************************
 *  Count Queries
 *    1. Create chart labels
 *    2. Query time, system, severity for all results from passed 24 hours from dnslog
 *    3. Use SQL rounding to round time to nearest 30 mins
 *    4. Count by 30 min time blocks into associative array
 *    5. Move values from associative array to daily count indexed array
 *
 *  Params:
 *    None
 *  Return:
 *    None
 */
function count_queries() {
  global $dbwrapper;
  global $allowed_queries, $blocked_queries, $chart_labels, $link_labels;
  global $day_allowed, $day_blocked;
  
  $allowed_arr = array();
  $blocked_arr = array();
  $currenttime = 0;
  $datestr = '';

  $currenttime = intval(time() - (time() % 1800)) + 3600;  //Round current time up to nearest 30 min period

  //Create labels, Allowed array, and Blocked array values
  for ($i = $currenttime - 84600; $i <= $currenttime; $i+=1800) {
    $datestr = date('H:i:00', $i);
    $allowed_arr[$datestr] = 0;
    $blocked_arr[$datestr] = 0;
    $chart_labels[] = date('H:i', $i);
    $link_labels[] = date('Y-m-d\TH:i:00', $i);
  }

  $hourlyvalues = $dbwrapper->queries_count_hourly($currenttime);
  
  if ($hourlyvalues === false) {                           //Leave if nothing found
    return false;
  }

  //Read each row of results totalling up a count per 30 min period depending whether the query was allowed / blocked
  foreach ($hourlyvalues as $row) {
    if ($row['severity'] == '1') {
      $allowed_arr[$row['round_time']]++;
      $day_allowed++;
    }
    elseif ($row['severity'] == '2') {
      $blocked_arr[$row['round_time']]++;
      $day_blocked++;
    }
  }

  $allowed_queries = array_values($allowed_arr);
  $blocked_queries = array_values($blocked_arr);
}


/********************************************************************
 *  Show Latest version alert if a newer version of NoTrack is available
 *
 *  Params:
 *    None
 *  Return:
 *    None
 */
function show_latestversion() {
  global $upgradenotifier;

  if ($upgradenotifier->is_upgrade_available()) {
    echo '<div class="alertupgrade">',
      'There is a newer version available - <a href="./upgrade.php">Update Now</a>',
      '</div>', PHP_EOL;
  }
}

/********************************************************************
 */

draw_page_header('v'.VERSION);
draw_page_nav();
echo '<main>', PHP_EOL;

count_queries();
show_latestversion();

echo '<div class="home-nav-container">', PHP_EOL;          //Start home-nav-container
home_status();
home_blocklist();
home_queries($day_allowed, $day_blocked);
home_network();

//home_sitesblocked();

echo '</div>', PHP_EOL;                                    //End home-nav-container
if ($day_allowed + $day_blocked > 0) {
  linechart($allowed_queries, $blocked_queries, $chart_labels, $link_labels, '/PT1H', 'DNS Queries over past 24 hours');
}

?>
</main>
</body>
</html>
