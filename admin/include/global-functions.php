<?php
//Global Functions used in NoTrack Admin

/********************************************************************
 *  Draw Sys Table DEPRECATED
 *    Start off a sys-group table
 *  Params:
 *    Description, Value
 *  Return:
 *    None
 */
function draw_sysrow($description, $value) {
  echo "<tr><td>{$description}: </td><td>{$value}</td></tr>".PHP_EOL;
}


/********************************************************************
 *  Activate Session
 *    Create login session
 *  Params:
 *    None
 *  Return:
 *    None
 */
function activate_session() {  
  $_SESSION['session_expired'] = false;
  $_SESSION['session_start'] = time();  
}

function ensure_active_session() {
  global $config;

  if ($config->is_password_protection_enabled()) {
    session_start();
    if (isset($_SESSION['session_start'])) {
      if (!is_active_session()) {
        $_SESSION['session_expired'] = true;
        header('Location: /admin/login.php');
        exit;
      }
    }
    else {
      header('Location: /admin/login.php');
      exit;
    }
  }
}


function is_active_session() {
  $session_duration = 1800;
  if (isset($_SESSION['session_start'])) {
    if ((time() - $_SESSION['session_start']) < $session_duration) {
      return true;
    }
  }
  return false;
}


/********************************************************************
 *  Extract Domain
 *    Extract domain with optional double-barrelled tld
 *  Params:
 *    URL to check
 *  Return:
 *    Filtered domain
 */
function extract_domain($url) {
  $regex_domain = '/[\w\-]{1,63}\.(org\.|co\.|com\.|net\.|gov\.)?[\w\-]{1,63}$/';

  if (preg_match($regex_domain, $url, $matches)) {
    return $matches[0];
  }
  return $url;
}


/**
 * Filter Domain
 *  Input validation to ensure a domain is fully qualified.
 *  NOTE custom blocklists page allows for country only.
 *
 *  @param str Domain to check
 *  @param bool Country only or FQDN
 *  @param mixed Default value
 *  @return mixed User input on success or default value on failure
 *
 */
function filter_domain($value, $allow_country_only, $defaultvalue) {
  $regexp = '';
  $userinput = trim($value);

  //Select regex
  if ($allow_country_only) {
    //Match either 1 label (.country) or multiple labels (some-site.com)
    $regexp = '/^([\w\-]{1,63}(\.[\w\-]{1,63})+|\.[\w\-]{1,63})$/';
  }
  else {
    //Match a FQDN - multiple labels (some-site.com)
    $regexp = '/^[\w\-]{1,63}(\.[\w\-]{1,63})+$/';
  }

  //Maximum length of DNS is 255, but includes two bytes for length
  if (strlen($userinput) > 253) {
    return $defaultvalue;
  }

  //Make sure the input isn't an IP address
  if (filter_var($userinput, FILTER_VALIDATE_IP) !== false) {
    return $defaultvalue;
  }

  //Finally carry out regex validation
  if (preg_match($regexp, $userinput)) {
    return $userinput;
  }
  else {
    return $defaultvalue;
  }
}


/**
 * Filter Integer Value
 *  Input validation to check if supplied value is numeric and in a range
 *
 *  @param int Value to check
 *  @param int Minimum
 *  @param int Maximum
 *  @param int Default value
 *  @return int value on success, default value on fail
 *
 */
function filter_integer($value, $min, $max, $defaultvalue=0) {
  if (is_numeric($value)) {
    if (($value >= $min) && ($value <= $max)) {
      return intval($value);
    }
  }

  return $defaultvalue;
}


/**
 * Filter IP Address
 *  Input validation to check for a valid IPv4 or IPv6 address
 *
 *  @param str Value to check
 *  @param str Default value
 *  @return str new value on success, default value on invalid input
 *
 */
function filter_ip($value, $defaultvalue) {
  $userinput = trim($value);
  $options = array(
    'options' => array('default' => $defaultvalue),
  );

  //Make sure input is less than an IPv6 address
  if (strlen($userinput) > 45) {
    return $defaultvalue;
  }

  return filter_var($userinput, FILTER_VALIDATE_IP, $options);
}


/********************************************************************
 *  Filter String
 *    Validates a string
 *
 *  Params:
 *    str (str): String to check
 *    maxlen (int): Maximum length of string
 *  Return:
 *    true on acceptable value
 *    false for unacceptable value
 */
function filter_string($str, $maxlen=255) {
  //Ensure string is below maximum length
  if (strlen($str) > $maxlen) {
    return false;
  }

  if (preg_match('/[<>]/', $str)) {                        //Check there are no HTML Tags
    return false;
  }

  return true;
}


/**
 * Filter URL
 *  Input validation to check if a url is valid
 *
 *  Regex:
 *    Group 1: FTP or HTTP
 *    Group 2: Domain
 *    Group 3: URI
 *
 *  @param str URL to check
 *  @return bool true on success, false on failure
 *
 */
function filter_url($url) {
  if (preg_match('/^(ftp|https?):\/\/([\w\-\.]{0,254})(\/[\w\-\.\/]*)?([\w#!:.?+=&%@!\-\/]+)?$/', $url)) {
    return true;
  }
  else {
    return false;
  }
}


/**
 * Format Number
 *  Returns a number rounded to 3 significant figures
 *
 *  @param int Number to round
 *  @return str Number rounded to 3sf
 *
 */
function formatnumber($number) {
  if ($number < 1000) return $number;
  elseif ($number < 10000) return number_format($number / 1000, 2).'k';
  elseif ($number < 100000) return number_format($number / 1000, 1).'k';
  elseif ($number < 1000000) return number_format($number / 1000, 0).'k';
  elseif ($number < 10000000) return number_format($number / 1000000, 2).'M';
  elseif ($number < 100000000) return number_format($number / 1000000, 1).'M';
  elseif ($number < 1000000000) return number_format($number / 1000000, 0).'M';
  elseif ($number < 10000000000) return number_format($number / 1000000000, 2).'G';
  elseif ($number < 100000000000) return number_format($number / 1000000000, 1).'G';
  elseif ($number < 1000000000000) return number_format($number / 1000000000, 0).'G';
  
  return number_format($number / 1000000000000, 0).'T';
}


/**
 * Load a JSON file and return the data as an array
 *
 *  @param string File Name
 *  @return array of data
 */
function load_json($filename) {
  $data = file_get_contents($filename);
  $jsondata = json_decode($data, JSON_OBJECT_AS_ARRAY);

  return $jsondata;
}


/********************************************************************
 *  Pluralise
 *
 *  Params:
 *    count, text
 *  Return:
 *    pluralised string
 */
function pluralise($count, $text)
{
  return $count.(($count == 1) ? (" {$text}") : (" {$text}s"));
}

/********************************************************************
 *  Simplified Time
 *    Returns a simplified time from now - $timestr
 *
 *  Params:
 *    date-time string
 *  Return:
 *    A simplified time string
 */
function simplified_time($timestr) {
  $datetime = new DateTime($timestr);
  $interval = date_create('now')->diff($datetime);

  $suffix = ($interval->invert ? ' ago' : '');
  if ($interval->y >= 1 ) return pluralise($interval->y, 'year').$suffix;
  if ($interval->m >= 1 ) return pluralise($interval->m, 'month').$suffix;
  if ($interval->d > 7) return pluralise(floor($interval->d / 7), 'week').$suffix;
  if ($interval->d >= 1) return pluralise($interval->d, 'day').$suffix;
  if ($interval->h >= 1 ) return pluralise($interval->h, 'hour').$suffix;
  if ($interval->i >= 1 ) return pluralise($interval->i, 'minute').$suffix;
  return pluralise($interval->s, 'second').$suffix;
}


/********************************************************************
 *  Is Checked
 *    Used to in forms to determine if tickbox should be checked
 *  Params:
 *    value
 *  Return:
 *    checked="checked" or nothing
 */
 function is_checked($value) {
  if ($value == 1 || $value === true) {
    return ' checked="checked"';
  }
  
  return '';
}


/**
 * Sanitize String to limit length of user input and remove spaces
 *
 *  @param string user input string to sanitize
 *  @param string default value to replace bad user input
 *  @param int maximum length string can be
 */
function sanitize_string($str, $defaultvalue, $maxlen) {
  //Reduce processing if user inputs an oversized string
  if (strlen($str) > $maxlen) {
    return $defaultvalue;
  }

  return filter_var($str, FILTER_SANITIZE_STRING | FILTER_FLAG_STRIP_LOW);
}


/********************************************************************
 *  Check SQL Table Exists DEPRECATED once removed from investigate.php
 *    Uses LIKE to check for table name in order to avoid error message.
 *  Params:
 *    SQL Table
 *  Return:
 *    True if table exists
 *    False if table does not exist
 */
function table_exists($table) {
  global $db;
  $exists = false;
  
  $result = $db->query("SHOW TABLES LIKE '$table'");
  
  if ($result->num_rows == 1) { 
    $exists = true;
  }
  
  $result->free();
  return $exists;
}


/********************************************************************
 *  Draw Line Chart
 *    Draws background line chart using SVG
 *    1. Calulate maximum values of input data for $ymax
 *    2. Draw grid lines
 *    3. Draw axis labels
 *    4. Draw coloured graph lines
 *    5. Draw coloured circles to reduce sharpness of graph line
 *
 *  Params:
 *    $values1 - array 1, $values2 array 2, $xlabels array
 *  Return:
 *    None
 */
function linechart($values1, $values2, $xlabels, $link_labels, $extraparams, $title) {
  $jump = 0;
  $max_value = 0;
  $ymax = 0;
  $xstep = 0;
  $pathout = '';
  $numvalues = 0;
  $x = 0;
  $y = 0;

//Prepare chart
  $max_value = max(array(max($values1), max($values2)));
  $numvalues = count($values1);
  $values1[] = 0;                                          //Ensure line returns to 0
  $values2[] = 0;                                          //Ensure line returns to 0
  $xlabels[] = $xlabels[$numvalues-1];                     //Increment xlables

  $xstep = 1900 / $numvalues;                              //Calculate x axis increment
  if ($max_value < 200) {                                  //Calculate y axis maximum
    $ymax = (ceil($max_value / 10) * 10) + 10;             //Change offset for low values
  }
  elseif ($max_value < 10000) {
    $ymax = ceil($max_value / 100) * 100;
  }
  else {
    $ymax = ceil($max_value / 1000) * 1000;
  }

  $jump = floor($numvalues / 12);                          //X Axis label and line gap

  echo '<div class="linechart-container">'.PHP_EOL;        //Start Chart container
  echo '<h2>'.$title.'</h2>';
  echo '<svg width="100%" height="100%" viewbox="0 0 2000 760">'.PHP_EOL;

  //Axis line rectangle with rounded corners
  echo '<rect class="axisline" width="1900" height="701" x="100" y="0" rx="5" ry="5" />'.PHP_EOL;

  for ($i = 0.25; $i < 1; $i += 0.25) {                    //Draw Y Axis lables and (horizontal lines)
    echo '<path class="gridline" d="M100,'.($i*700).' H2000" />'.PHP_EOL;
    echo '<text class="axistext" x="8" y="'.(18+($i*700)).'">'.formatnumber((1-$i)*$ymax).'</text>'.PHP_EOL;
  }
  echo '<text x="8" y="705" class="axistext">0</text>';
  echo '<text x="8" y="38" class="axistext">'.formatnumber($ymax).'</text>';
  
  
  for ($i = 0; $i < $numvalues; $i += $jump) {             //Draw X Axis and labels (vertical lines)
    echo '<text x="'.(60+($i * $xstep)).'" y="746" class="axistext">'.$xlabels[$i].'</text>'.PHP_EOL;
    echo '<path class="gridline" d="M'.(100+($i*$xstep)).',2 V700" />'.PHP_EOL;
  }
  
  draw_graphline($values1, $xstep, $ymax, '#00b7ba');
  draw_graphline($values2, $xstep, $ymax, '#b1244a');

  //Draw circles over line points in order to smooth the apperance
  for ($i = 1; $i < $numvalues; $i++) {
    $x = 100 + (($i) * $xstep);                            //Calculate X position

    if ($values1[$i] > 0) {                                //$values1[] (Allowed)
      $y = 700 - (($values1[$i] / $ymax) * 700);           //Calculate Y position of $values1
      echo '<a href="./queries.php?datetime='.$link_labels[$i].$extraparams.'&amp;groupby=time&amp;sort=ASC&amp;severity=1" target="_blank">'.PHP_EOL;
      echo '  <circle cx="'.$x.'" cy="'.(700-($values1[$i]/$ymax)*700).'" r="10px" fill="#00b7ba" fill-opacity="1" stroke="#EAEEEE" stroke-width="4px">'.PHP_EOL;
      echo '    <title>'.$xlabels[$i].' '.$values1[$i].' Allowed</title>'.PHP_EOL;
      echo '  </circle>'.PHP_EOL;
      echo '</a>'.PHP_EOL;
    }

    if ($values2[$i] > 0) {                                //$values2[] (Blocked)
      $y = 700 - (($values2[$i] / $ymax) * 700);           //Calculate Y position of $values2
      echo '<a href="./queries.php?datetime='.$link_labels[$i].$extraparams.'&amp;groupby=time&amp;sort=ASC&amp;severity=2" target="_blank">'.PHP_EOL;
      echo '  <circle cx="'.$x.'" cy="'.(700-($values2[$i]/$ymax)*700).'" r="10px" fill="#b1244a" fill-opacity="1" stroke="#EAEEEE" stroke-width="4px">'.PHP_EOL;
      echo '    <title>'.$xlabels[$i].' '.$values2[$i].' Blocked</title>'.PHP_EOL;
      echo '  </circle>'.PHP_EOL;
      echo '</a>'.PHP_EOL;
    }
  }
  
  echo '</svg>'.PHP_EOL;                                   //End SVG
  echo '</div>'.PHP_EOL;                                   //End Chart container

}


/********************************************************************
 *  Draw Graph Line
 *    Calulates and draws the graph line using straight point-to-point notes
 *
 *  Params:
 *    $values array, x step, y maximum value, line colour
 *  Return:
 *    None
 */

function draw_graphline($values, $xstep, $ymax, $colour) {
  $path = '';
  $x = 0;                                                  //Node X
  $y = 0;                                                  //Node Y
  $numvalues = count($values);
  
  $path = "<path d=\"M 100,700 ";                          //Path start point
  for ($i = 1; $i < $numvalues; $i++) {
    $x = 100 + (($i) * $xstep);
    $y = 700 - (($values[$i] / $ymax) * 700);
    $path .= "L $x $y";
  }
  $path .= 'V700 " stroke="'.$colour.'" stroke-width="5px" fill="'.$colour.'" fill-opacity="0.15" />'.PHP_EOL;
  echo $path;
}
?>
