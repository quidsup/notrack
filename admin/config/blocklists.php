<?php
require('../include/global-vars.php');
require('../include/global-functions.php');
require('../include/config.php');
require('../include/menu.php');
require('../include/mysqlidb.php');

ensure_active_session();

/************************************************
*Global Variables                               *
************************************************/
$dbwrapper = new MySqliDb;

/************************************************
*Arrays                                         *
************************************************/
$blstats = array();
$list = array();                                 //Global array for all the Block Lists


/**
 * Format a table row for block lists
 *  @param string Block List Name
 *  @param array JSON data for the Block List
 *  @return string HTML formtted table row
 */
function blocklist_row($blname, $bl) {
  global $config;

  $checked = '';
  $checkbox = '';
  $desc = '';
  $flag = '';
  $flagclass = '';
  $flagword = '';
  $links = '';
  $row = '';
  $stats = '';
  $title = '';

  if ($config->is_blocklist_active($blname)) {
    $checked = ' checked="checked"';
    $stats = get_blocklist_stats($blname);
    if ($stats === false) {                               //No stats found
      $stats = '';
    }
    else {                                                //Include stats for block list
      $stats = "<i>{$stats[1]} used of {$stats[0]}</i>";
    }
  }

  $checkbox = '<input type="checkbox" name="'.$blname.'"'.$checked.' onChange="setBlocklist(this)">';

  $flagclass = $bl['flagclass'] ?? 'flag-icon-orange';
  $flagword = $bl['flag'] ?? substr($bl['title'], 0, 2);
  $flag = "<div class=\"flag-icon {$flagclass}\">{$flagword}</div>";

  $title = $bl['title'].':';
  $desc = $bl['desc_en'];
  $links = '<a href="'.$bl['home'].'" target="_blank" title="Home"><img alt="Home" src="../svg/icon_home.svg"></a>';
  if (array_key_exists('info', $bl)) {
    //Include info link if it exists
    $links .= '<a href="'.$bl['info'].'" target="_blank" title="Info"><img alt="Info" src="../svg/icon_info.svg"></a>';
  }

  $row = "<tr><td>{$flag}</td><td>{$checkbox}</td><td>{$title}</td><td>{$desc}{$links}{$stats}</td>";

  return $row;
}


/**
 * Get stats for a block list
 *
 *  @param string Block List Name
 *  @return array containing stats on success or false on failure
 */
function get_blocklist_stats($blname) {
  global $blstats;

  if (array_key_exists($blname, $blstats)) {
    return ($blstats[$blname]);
  }
  else {
    return false;
  }
}


/**
 * Build Block List Totals Array
 *  Query all results from blockliststats table
 *  Rebuild multidimensional array into an associative array
 */
function build_blocklist_totals() {
  global $blstats, $dbwrapper;

  $result = $dbwrapper->blockliststats_read();

  if ($result === false) {                                //Any data returned?
    return;
  }

  foreach ($result as $line) {                            //Rebuild array
    $blstats[$line['bl_source']] = array($line['filelines'], $line['linesused']);
  }
}


/**
 * Custom Block Lists
 *
 *  @note a save button is required here
 */
function custom_blocklists() {
  global $config;

  echo '<div>', PHP_EOL;                                  //Start Tab
  echo '<h5>Custom Block Lists</h5>', PHP_EOL;
  echo '<p>Use either Downloadable or Localy stored Block Lists</p><textarea rows="18" name="bl_custom">', $config->get_blocklist_custom(), '</textarea>', PHP_EOL;

  echo '<table class="bl-table">', PHP_EOL;
  echo '<tr><td><button type="submit" class="material-icon-button-text icon-tick" name="v" value="8">Save Changes</button></td></tr>', PHP_EOL;
  echo '</table>', PHP_EOL;                              //End bl table
  echo '</div>', PHP_EOL;                                //End Tab
}


/**
 * Update Blocks
 *  1: Set bl_custom by splitting and filtering values from POST[bl_custom]
 *  2: Save the config file
 *
 */
function update_blocklists() {
  global $config;

  $customstr = '';                                        //Filtered string of POST bl_custom
  $customlist = array();                                  //Array of items from customstr
  $validlist = array();                                   //Valid items from customlist

  $customstr = $_POST['bl_custom'] ?? '';

  if (filter_string($customstr, 2000)) {
    //Split array
    $customstr = preg_replace('#\s+#',',',trim(strip_tags($customstr)));
    $customlist = explode(',', $customstr);                //Split string into array

    //Check if each item is a valid URL or file location?
    foreach ($customlist as $site) {
      if (filter_url($site)) {
        $validlist[] = strip_tags($site);
      }
      elseif (preg_match('/\/\w{3,4}\/[\w\/\.]/', $site)) {
        $validlist[] = strip_tags($site);
      }
    }

    //Are there any items in the valid list?
    if (sizeof($validlist) == 0) {
      //No - blank out bl_custom
      $config->set_blocklist_custom('');
    }
    else {
      //Yes - save as comma seperated values
      $config->set_blocklist_custom(implode(',', $validlist));
    }
  }
  else {
    $config->set_blocklist_custom('');                     //Nothing set - blank bl_custom
  }

  $config->save_blocklists();
}


/**
 * Draw Tabbed View
 *  This function is called when a value is set for GET/POST argument "v"
 *  1. Check which tab to set as checked
 *  2. Draw the tabbed elements
 *  3. Draw each block list table
 *
 *  @param int Tab to view
 *
 */
function draw_tabbedview($view) {
  $tab = filter_integer($view, 1, 8, 1);
  $checkedtabs = array('', '', '', '', '', '', '', '', '');
  $checkedtabs[$tab] = ' checked';

  echo '<form name="blocklists" action="?" method="post">', PHP_EOL;
  echo '<input type="hidden" name="action" value="blocklists">', PHP_EOL;
  echo '<input type="hidden" name="v" value="'.$tab.'">', PHP_EOL;

  echo '<section>', PHP_EOL;
  echo '<div id="tabbed">', PHP_EOL;                      //Start tabbed container

  echo '<input type="radio" name="tabs" id="tab-nav-1"', $checkedtabs[1], '><label for="tab-nav-1">Tracking</label>', PHP_EOL;
  echo '<input type="radio" name="tabs" id="tab-nav-2"', $checkedtabs[2], '><label for="tab-nav-2">Advertising</label>', PHP_EOL;
  echo '<input type="radio" name="tabs" id="tab-nav-3"', $checkedtabs[3], '><label for="tab-nav-3">Malware</label>', PHP_EOL;
  echo '<input type="radio" name="tabs" id="tab-nav-4"', $checkedtabs[4], '><label for="tab-nav-4">Crypto Coin</label>', PHP_EOL;
  echo '<input type="radio" name="tabs" id="tab-nav-5"', $checkedtabs[5], '><label for="tab-nav-5">Social</label>', PHP_EOL;
  echo '<input type="radio" name="tabs" id="tab-nav-6"', $checkedtabs[6], '><label for="tab-nav-6">Multipurpose</label>', PHP_EOL;
  echo '<input type="radio" name="tabs" id="tab-nav-7"', $checkedtabs[7], '><label for="tab-nav-7">Regional</label>', PHP_EOL;
  echo '<input type="radio" name="tabs" id="tab-nav-8"', $checkedtabs[8], '><label for="tab-nav-8">Custom</label>', PHP_EOL;

  echo '<div id="tabs">', PHP_EOL;                        //Start Tabs

  $tabledata = prepare_tables();                          //Load JSON Block list template
  draw_table('Tracker', $tabledata['tracker']);           //Output the processed data
  draw_table('Advertising', $tabledata['advert']);
  draw_table('Malware', $tabledata['malware']);
  draw_table('Crypto Coin', $tabledata['cryptocoin']);
  draw_table('Social', $tabledata['social']);
  draw_table('Multipurpose', $tabledata['multi']);
  draw_table('Regional', $tabledata['regional']);
  custom_blocklists();

  echo '</div>', PHP_EOL;                                 //End tabs
  echo '</div>', PHP_EOL;                                 //End tabbed container
  echo '</section>', PHP_EOL;
  echo '</form>', PHP_EOL;                                //End form
}


/**
 * Draw Table
 *  Output the processed data from block list template into a table
 *
 *  @param str Section Title
 *  @param array Table Data
 *
 */
function draw_table($title, $tabledata) {
  echo '<div>', PHP_EOL;                                  //Start Tab
  echo "<h5>{$title} Block List:</h5>", PHP_EOL;
  echo '<table class="bl-table">', PHP_EOL;
  foreach ($tabledata as $tablerow) {
    echo $tablerow, PHP_EOL;
  }
  echo '</table>', PHP_EOL;
  echo '</div>', PHP_EOL;                                 //End Tab
}


/**
 * Draw Welcome
 *  Draw Welcome is called when no value has been set for GET/POST argument "v"
 *
 */
function draw_welcome() {
  echo '<section>', PHP_EOL;
  echo '<div class="bl-flex-container">', PHP_EOL;
  echo '<a href="?v=1"><img src="../svg/homebl_tracking.svg" alt=""><h6>Tracking</h6></a>', PHP_EOL;
  echo '<a href="?v=2"><img src="../svg/homebl_advertising.svg" alt=""><h6>Advertising</h6></a>', PHP_EOL;
  echo '<a href="?v=3"><img src="../svg/homebl_malware.svg" alt=""><h6>Malware</h6></a>', PHP_EOL;
  echo '<a href="?v=4"><img src="../svg/homebl_cryptocoin.svg" alt=""><h6>Crypto Coin</h6></a>', PHP_EOL;
  echo '<a href="?v=5"><img src="../svg/homebl_social.svg" alt=""><h6>Social</h6></a>', PHP_EOL;
  echo '<a href="?v=6"><img src="../svg/homebl_multipurpose.svg" alt=""><h6>Multipurpose</h6></a>', PHP_EOL;
  echo '<a href="?v=7"><img src="../svg/homebl_regional.svg" alt=""><h6>Regional</h6></a>', PHP_EOL;
  echo '<a href="?v=8"><img src="../svg/homebl_custom.svg" alt=""><h6>Custom</h6></a>', PHP_EOL;
  echo '</div>', PHP_EOL;
  echo '</section>', PHP_EOL;
}

/**
 * Prepare Tables
 *  1. Load blocklisttemplate.json
 *  2. Append formatted row HTML code to the table array specified by the type value
 *
 */
function prepare_tables() {
  //Array of block lists types in order to build the HTML tables
  $tabledata = array(
    'tracker' => array(),
    'advert' => array(),
    'malware' => array(),
    'cryptocoin' => array(),
    'social' => array(),
    'multi' => array(),
    'regional' => array(),
  );

  $blocklist_template = load_json('../include/blocklisttemplate.json');

  foreach($blocklist_template as $blname => $bl) {
    //Skip custom block lists as they aren't required for this table view
    if ($blname == 'bl_allowlist') continue;
    if ($blname == 'bl_blacklist') continue;
    if ($blname == 'bl_tld') continue;

    //Append formatted row HTML code to the table array specified by the type value
    $tabledata[$bl['type']][] = blocklist_row($blname, $bl);
  }

  return $tabledata;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=0.9">
  <link href="../css/master.css" rel="stylesheet" type="text/css">
  <link href="../css/flags.css" rel="stylesheet" type="text/css">
  <link href="../css/icons.css" rel="stylesheet" type="text/css">
  <link href="../css/tabbed.css" rel="stylesheet" type="text/css">
  <link rel="icon" type="image/png" href="../favicon.png">
  <script src="../include/menu.js"></script>
  <title>NoTrack - Block Lists</title>
</head>

<body>
<?php
draw_page_header('Block Lists');
draw_page_nav();

$config->load_blocklists();
build_blocklist_totals();                                  //Build blstats array

echo '<main>', PHP_EOL;

//Has the Save Changes button been clicked?
if (isset($_POST['action'])) {                             //Yes - Update block list conf
  update_blocklists();
}

if (isset($_GET['v'])) {
  draw_tabbedview($_GET['v']);
}
elseif (isset($_POST['v'])) {
  draw_tabbedview($_POST['v']);
}
else {
  draw_welcome();
}

?>

<div id="copymsg">clipboard</div>
</main>

<script>
function setBlocklist(box) {
  var xhr = new XMLHttpRequest();
  var url = '/admin/include/api.php';
  var params = 'operation=blocklist_status&blname=' + box.name + '&blstatus=' + box.checked;

  xhr.open('POST', url, true);
  xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

  xhr.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      //console.log(this.responseText);
      var apiResponse = JSON.parse(this.responseText);

      document.getElementById('copymsg').innerText = apiResponse['message'];
      document.getElementById('copymsg').style.display = 'block';

      //Delay for 3 seconds, then Hide copymsg element
      setTimeout(function(){
        document.getElementById('copymsg').style.display = 'none';
      },3000);
    }
  }
  xhr.send(params);
}
</script>
</body>
</html>
