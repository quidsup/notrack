<?php
define('REGEX_ANON', '/(id|src)=/');
//Look for a component ending in id or src

define('REGEX_URL', '/https?:\/\/?[\w\-\.]{3,256}(\/[\w\-\.~:;\#\[\]@!\$&\'\(\)\*\+,%=\|\/]*)?(\?[\w\-\.~:;\#\[\]@!\$&\'\(\)\*\+,%=\|\/]*)?/');

$notrackadmin = '';
$urllist = array();                                        //List of URLs
$paramlist = array();                                      //Sorted list of GET args

if (is_file('./sinksettings.php')) {
  require_once './sinksettings.php';
}

/********************************************************************
 *  Extract URLs
 *    Strip tags from HTTP GET values then store them in $paramlist
 *    Check for any URLs
 *
 *  Params:
 *    None
 *  Return:
 *    None
 */
function extract_urls() {
  global $paramlist, $urllist;
  $taglessvalue = '';                                      //Value with HTML tags removed by strip_tags
  $matches = array();

  foreach($_GET as $key => $value) {                       //Go through each HTTP GET parameter
    $taglessvalue = strip_tags($value);                    //Prevent XSS

    if ($key != 'ntrkorigin') {                            //Skip ntrkorigin
      $paramlist[strip_tags($key)] = $taglessvalue;        //Store parameter
    }

    //Is the taglessvalue a URL?
    if (preg_match(REGEX_URL, $taglessvalue, $matches)) {
      if (! in_array($matches[0], $urllist)) {             //Prevent duplicates
        $urllist[] = $matches[0];                          //Add array to list
        anonymise_url($matches[0]);
      }
    }
  }
}


/********************************************************************
 *  Extract URLs
 *    Strip any parameters which look like a source or tracking ID from a URL
 *    Based on the presence of ID or SRC in a parameter
 *
 *  Params:
 *    $url - URL to check
 *  Return:
 *    None
 */
function anonymise_url($url) {
  global $urllist;
  $anonurl = '';                                           //Resultant anonymised URL
  $query = '';
  $modified = false;

  $components = array();
  $queryitems = array();

  $components = parse_url($url);                           //Split URL

  $anonurl = $components['scheme'] ?? 'https';             //Reassemble with the expectation of dropping items from components
  $anonurl .= '://';
  $anonurl .= $components['host'] ?? '';
  $anonurl .= $components['path'] ?? '';
  $anonurl .= '?';
  $query = $components['query'] ?? '';

  if ($query == '') {                                      //Any query parameters to analyse
    return;                                                //None - Leave this function
  }

  $queryitems = explode('&', $query);                      //Explode by expected ampersand seperator

  foreach($queryitems as $item) {
    //Check for id or src in query parameter
    if (preg_match(REGEX_ANON, $item)) {
      $modified = true;                                    //Found - set modified to true and drop parameter
    }
    else {                                                 //Something else, add to url
      $anonurl .= $item.'&';
    }
  }

  //Only add a modified (anonymised) URL to urllist
  if ($modified) {
    $urllist[] = rtrim($anonurl, '?&');                    //Remove surplus ? or &
  }
}

/**
 * Draw Actions
 *  Add a back button and unblock button
 *
 * @param str Domain to unblock
 * @param str NoTrack server URL
 *
 */
function draw_actions($domain, $notrackadmin) {
  $allow_title = "Add {$domain} to NoTrack Allow List";
  $customblurl = "{$notrackadmin}/admin/config/customblocklist.php?v=allow";

  echo '<div>', PHP_EOL;
  echo '<button type="button" onclick="history.back()">Go Back</button>', PHP_EOL;
  echo '</div>', PHP_EOL;

  if ($notrackadmin == '') return;
  echo '<div>', PHP_EOL;
  echo '<form action="', $customblurl, '" method="POST" target="_blank">', PHP_EOL;
  echo '<input type="hidden" name="action" value="allow">', PHP_EOL;
  echo '<input type="hidden" name="singledomain" value="', $domain, '">', PHP_EOL;
  echo '<input type="hidden" name="comment" value="Added via Sink page">', PHP_EOL;
  echo '<button type="submit" title="', $allow_title, '">Add to Allow List</button>', PHP_EOL;
  echo '</form>', PHP_EOL;
  echo '</div>', PHP_EOL;
}

/********************************************************************
 *  Draw the Extracted URLs
 *
 *  Params:
 *    None
 *  Return:
 *    None
 */
function draw_urllist() {
  global $urllist;

  if (count($urllist) == 0) {                              //Any URLs extracted?
    return;
  }

  echo '<h4>Extracted URL&rsquo;s:</h4>'.PHP_EOL;
  echo '<ul>'.PHP_EOL;
  foreach($urllist as $url) {
    echo "<li><a href=\"{$url}\">{$url}</a></li>".PHP_EOL;
  }
  echo '</ul>'.PHP_EOL;
}


/********************************************************************
 *  Draw the Extracted Parameters
 *
 *  Params:
 *    None
 *  Return:
 *    None
 */
function draw_paramlist() {
  global $paramlist;
  $i = 1;                                                  //Beautify

  echo '<h4>Parameter list:</h4>'.PHP_EOL;

  if (count($paramlist) == 0) {                            //Any parameters extracted?
    echo 'None specified<br>'.PHP_EOL;
    return;
  }

  ksort($paramlist);

  echo '<table>'.PHP_EOL;
  foreach($paramlist as $key => $value) {
    echo "<tr><td>{$i}</td><td>{$key}</td><td>{$value}</td></tr>".PHP_EOL;
    $i++;
  }
  echo '</table>'.PHP_EOL;
}
$host = $_SERVER['HTTP_HOST'];
$ntrkorigin = $_GET['ntrkorigin'] ?? '';
$ntrkorigin = strip_tags($ntrkorigin);                     //XSS Protection

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link href="./sink.css" rel="stylesheet" type="text/css">
  <meta name="viewport" content="width=device-width, initial-scale=0.9">
  <title>Blocked by NoTrack</title>
</head>
<body>
<div class="circle1"></div>
<div class="circle2"></div>
<section>

<?php
echo '<h1>Blocked by NoTrack</h1>'.PHP_EOL;
echo '<h2>Sorry about that</h2>'.PHP_EOL;
echo "<h3><a href=\"https://{$host}${ntrkorigin}\">{$host}${ntrkorigin}</a></h3>".PHP_EOL;

extract_urls();
draw_urllist();
draw_actions($host, $notrackadmin);
draw_paramlist();

?>
</section>
</body>
</html>
