#!/usr/bin/env python3
#Title       : NoTrack Upgrade
#Description : This script carries out upgrade for NoTrack
#Author      : QuidsUp
#Date        : 2020-03-28
#Version     : 22.09
#Usage       : sudo python3 ntrkupgrade.py

#Standard imports
import shutil
import subprocess

#Local imports
import share
import folders
import regex
from ntrkservices import Services

#Create logger
logger = share.createlogger('ntrkupgrade')

class NoTrackUpgrade():
    """
    Class to carry out upgrade of NoTrack
    Finds where NoTrack is installed based on current working directory
    Finds the username for NoTrack install location
    Downloads latest version of NoTrack either via Git or HTTPS download from Gitlab
    Copies new webadmin files to /var/www/html/admin
    Copies new sink files to /var/www/html/sink
    """
    location: str
    username: str
    _git_download: str
    _repo: str
    _latestversion: str = ''
    _webowner: object

    def __init__(self):
        #Set folder locations
        self._repo = 'https://gitlab.com/quidsup/notrack.git'
        self._git_download = 'https://gitlab.com/quidsup/notrack/-/archive/master/notrack-master.zip'

        self._webowner = share.get_owner(folders.webdir)

        self.location = share.find_notrack()          #Where has NoTrack been installed?

        self.username = share.find_username(self.location)

        if self.location == '':
            logger.error('Unable to find NoTrack location')
        else:
            logger.info(f'NoTrack Location: {self.location}')

        logger.info(f'NoTrack Username: {self.username}')

        self.__find_latest_version()


    def __find_latest_version(self) -> None:
        """
        Get latest version from bl_notrack.txt
        """
        tempbl: str = f'{folders.tempdir}/bl_notrack.txt'
        filelines = list()

        filelines = share.load_file(tempbl)

        if len(filelines) == 0:
            logger.warning(f'{tempbl} is missing. This could be due to a recent reboot, or NoTrack block list is not enabled')
            return

        for line in filelines:
            #Use regex to find the correct line
            matches = regex.NTVersion.match(line)
            if matches is not None:
                self._latestversion = matches[1]
                logger.info(f'Latest version of NoTrack is: {self._latestversion}')
                break


    def __notification_create(self) -> None:
        """
        Create latestversion.php with the necessary php code
        """
        latestversionphp = f'{folders.webconfigdir}/latestversion.php'

        with open (latestversionphp, 'w') as f:
            f.write('<?php\n')
            f.write(f"$upgradenotifier->latestversion = '{self._latestversion}';\n")
            f.write('?>\n')
            f.close()                                      #Close latestversion.php

        share.set_owner(latestversionphp, self._webowner.st_uid, self._webowner.st_gid, 0o664)


    def __notification_delete(self) -> None:
        """
        Delete latestversion.php
        """
        logger.info('Deleting upgrade notification')
        share.delete(f'{folders.webconfigdir}/latestversion.php')


    def __check_git(self) -> None:
        """
        Checks if git is available
        """
        return shutil.which('git')


    def __git_clone(self) -> bool:
        """
        Attempt a Git Clone as a fallback when Git pull fails
        Switch from using root to appropriate username

        Returns:
            True on success
            False when Git has returned a non-zero code
        """
        cmd: list                                          #Command to execute

        cmd = ['sudo', '-u', self.username, 'git', 'clone', self._repo, self.location]
        logger.info(f'Cloning NoTrack into {self.location} with Git')

        p = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)

        if p.returncode == 0:                              #Success
            logger.info(p.stdout)
            return True
        elif p.returncode == 1:                            #Fatal Error
            logger.error(p.stderr)
            logger.error('Fatal error with Git Clone')
            return False

        logger.error(p.stderr)                             #Something wrong
        return False


    def __git_pull(self) -> bool:
        """
        Attempt a Git Pull
        Switch from using root to appropriate username

        Returns:
            True on success
            False when Git has returned a non-zero code
        """
        cmd: list                                          #Command to execute

        cmd = ['sudo', '-u', self.username, 'git', 'pull']
        logger.info('Pulling latest changes')

        p = subprocess.run(cmd, cwd=self.location, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)

        if p.returncode == 0:                              #Success
            logger.info(p.stdout)
            return True
        elif p.returncode == 1:                            #Fatal Error
            logger.error(p.stderr)
            logger.error('Fatal error with Git Pull')
            return False

        logger.error(p.stderr)                             #Something wrong

        return self.__git_clone()                          #Try to clone instead


    def __update_latest_version(self) -> bool:
        """
        Update PHP latest version setting file

        Returns:
            True - New version available
            False - Running current version
        """
        if self._latestversion == '':                    #Failed to get latest version
            return False

        if self._latestversion == share.VERSION:         #Already latest version
            logger.info(f'Running current version {share.VERSION}')
            return False

        logger.info(f'New version available: {self._latestversion}')
        logger.info('Updating latestversion.php')
        with open (self.__WEBCONFDIR + 'latestversion.php', 'w') as f:
            f.write('<?php\n')
            f.write("$config->set_latestversion('%s');\n" % self._latestversion)
            f.write('?>\n')
            f.close()                                      #Close file

        os.chmod(self.__WEBCONFDIR + 'latestversion.php', 0o664) #-rw-rw-r

        return True                                        #New version updated at this point


    def __copy_webfiles(self) -> None:
        """
        Copy new files to admin and sink folders
        """
        sinkdir = ''
        sinkdir = folders.webdir.replace('admin', 'sink', 1)

        logger.info('Copying new files to webadmin')
        share.copy(f'{self.location}/admin/config', f'{folders.webdir}/config')
        share.copy(f'{self.location}/admin/css', f'{folders.webdir}/css')
        share.copy(f'{self.location}/admin/help', f'{folders.webdir}/help')
        share.copy(f'{self.location}/admin/include', f'{folders.webdir}/include')
        share.copy(f'{self.location}/admin/svg', f'{folders.webdir}/svg')

        share.copy(f'{self.location}/admin/analytics.php', f'{folders.webdir}/analytics.php')
        share.copy(f'{self.location}/admin/dhcp.php', f'{folders.webdir}/dhcp.php')
        share.copy(f'{self.location}/admin/favicon.png', f'{folders.webdir}/favicon.png')
        share.copy(f'{self.location}/admin/index.php', f'{folders.webdir}/index.php')
        share.copy(f'{self.location}/admin/investigate.php', f'{folders.webdir}/investigate.php')
        share.copy(f'{self.location}/admin/live.php', f'{folders.webdir}/live.php')
        share.copy(f'{self.location}/admin/login.php', f'{folders.webdir}/login.php')
        share.copy(f'{self.location}/admin/logout.php', f'{folders.webdir}/logout.php')
        share.copy(f'{self.location}/admin/queries.php', f'{folders.webdir}/queries.php')
        share.copy(f'{self.location}/admin/upgrade.php', f'{folders.webdir}/upgrade.php')
        share.set_owner(folders.webdir, self._webowner.st_uid, self._webowner.st_gid, 0o664)

        share.copy(f'{self.location}/sink', sinkdir)
        share.set_owner(sinkdir, self._webowner.st_uid, self._webowner.st_gid, 0o664)


    def is_upgrade_available(self) -> bool:
        """
        Compare the current version against the latest version
        Use integer comparison and remove the dots from the string version
        """
        intcurrent: int = 0
        intlatest: int = 0

        if self._latestversion == '':                    #Missing latest version?
            logger.warning('Unable to determine if a new version of NoTrack is available')
            return False

        intcurrent = int(share.VERSION.replace('.', ''))
        intlatest = int(self._latestversion.replace('.', ''))

        if intlatest == intcurrent:                        #Equal to Gitlab version
            logger.info('Already running latest version of NoTrack')
            self.__notification_delete()
            return False
        elif intlatest > intcurrent:                       #Behind Gitlab version
            logger.info(f'New version of NoTrack available {self._latestversion}')
            self.__notification_create()
            return True
        else:                                              #Ahead of Gitlab version
            logger.info('Latest version of NoTrack is earlier than your version, ignoring')
            self.__notification_delete()
            return False


    def do_upgrade(self) -> None:
        """
        Do Upgrade
        1. Download latest updates
        2. Copy admin and sink files
        3. Delete notification of new version
        """
        logger.info('Upgrading NoTrack')

        if self.__check_git():
            if not self.__git_pull():
                logger.warning('Code download failed, unable to continue')
                return

        self.__copy_webfiles()
        self.__notification_delete()


    def delete_old_files(self) -> None:
        """
        Remove deprecated files
        """
        #Files deleted in NoTrack 22.09
        share.delete(f'{folders.webdir}/config/apisetup.php')
        share.delete(f'{folders.webdir}/include/tld.csv')


def main():
    logger.setLevel(20)                                   #Change log level to INFO
    share.logger.setLevel(20)

    print('NoTrack Upgrader')
    services = Services()
    ntrkupgrade = NoTrackUpgrade()

    share.userutils.check_root()
    ntrkupgrade.is_upgrade_available()
    ntrkupgrade.do_upgrade()
    ntrkupgrade.delete_old_files()

    print('Restarting NoTrack...')
    services.restart_notrack()

    print('NoTrack upgrade complete :-)')

if __name__ == "__main__":
    main()


